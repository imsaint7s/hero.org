if (window.innerWidth > 767) {
  var prevIndex = 1;
  onePageScroll(".hero-sections", {
    sectionContainer: "section",
    animationTime: 500,
    pagination: true,
    responsiveFallback: false,
    beforeMove: function(index) {
      if (prevIndex<index) {
        $('body').removeClass('scroll-up');
        $('body').addClass('scroll-down');
      } else {
        $('body').removeClass('scroll-down');
        $('body').addClass('scroll-up');
      }
      prevIndex = index;
    }
  });
}


var swiper = new Swiper('.hero-swiper', {
  speed: 500,
  slidesPerView: 3,
  slidesPerGroup: 1,
  loopFillGroupWithBlank: true,
  keyboard: {
    enabled: true,
  },
  navigation: {
    nextEl: '.swiper-button-next',
    prevEl: '.swiper-button-prev',
  },
  breakpoints: {
    1024: {
      slidesPerView: 2
    },
    768: {
      slidesPerView: 1
    }
  }
});
$('.hero-swiper .hero-link-preico').click(function(){
  swiper.slideTo(0);
});
$('.hero-swiper .hero-link-ico').click(function(){
  swiper.slideTo(6);
});


var teamSwiper = new Swiper('.hero-team-swiper', {
  speed: 500,
  slidesPerView: 4,
  slidesPerGroup: 1,
  spaceBetween: 3,
  loopFillGroupWithBlank: true,
  keyboard: {
    enabled: true,
  },
  navigation: {
    nextEl: '.swiper-button-next',
    prevEl: '.swiper-button-prev',
  },
  breakpoints: {
    1100: {
      slidesPerView: 3
    },
    920: {
      slidesPerView: 2
    },
    480: {
      slidesPerView: 1
    }
  }
});


$(document).ready(function(){
  document.querySelector('#hero-video').play();

  // replace href in hero-nav
  if ((window.innerWidth < 1137)&&(window.innerWidth > 767)) {
    $('.hero-nav a').click(function(){
      $('.hero-header-right').removeClass('hero-nav-show');
    });
  }

  if (window.innerWidth < 768) {
    $(".hero-nav a").each(function(){
      if(!$(this).hasClass('hero-wp-link')) {
        var anchor = $(this).attr('data-id');
        $(this).attr('href', '#'+anchor);
      }
    });
    $('.hero-nav a').click(function(evn){
      if(!$(this).hasClass('hero-wp-link')) {
        evn.preventDefault();
        $('html,body').scrollTo(this.hash, this.hash);
        $('.hero-header-right').removeClass('hero-nav-show');
      }
    });
  }

  var lang = $('html').attr('lang');
  $('.hero-lang a[data-lang="'+lang+'"]').addClass('hero-current-lang');

  $('.hero-lang').click(function() {
    $(this).toggleClass('hero-lang-show');
  });
  $(document).click(function(e) {
    if(!$(e.target).closest('.hero-lang-show').length) {
      $('.hero-lang').removeClass('hero-lang-show');
    };
  
    $(document).keydown(function(e) {
      if(e.keyCode === 27) {
        $('.hero-lang').removeClass('hero-lang-show');
      }
    });
  });

  $('.hero-link-join-us').click(function(){
    $('#hero-popup-join-us').addClass('hero-popup-show');
    $('.hero-header-right').removeClass('hero-nav-show');
  });
  $('#hero-popup-join-us .hero-popup-close').click(function(){
    $('#hero-popup-join-us').removeClass('hero-popup-show');
  });
  $(document).click(function(e) {
    /*if(!$(e.target).closest('#hero-popup-join-us').length) {
      // $('#hero-popup-join-us').removeClass('hero-popup-show');
    };*/  
    $(document).keydown(function(e) {
      if(e.keyCode === 27) {
        $('#hero-popup-join-us').removeClass('hero-popup-show');
      }
    });
  });

  $('#hero-hamburger').click(function(){
    $('.hero-header-right').addClass('hero-nav-show');
  });
  $('.hero-header-right .hero-popup-close').click(function(){
    $('.hero-header-right').removeClass('hero-nav-show');
  });
  $(document).click(function(e) {
    $(document).keydown(function(e) {
      if(e.keyCode === 27) {
        $('.hero-header-right').removeClass('hero-nav-show');
      }
    });
  });

  
  // timer
  var countDownDate = new Date("Jan 29, 2018 15:00:00").getTime();
  var titleDays = $(".hero-timer-days").text();
  var titleHours = $(".hero-timer-hours").text();
  var titleMinutes = $(".hero-timer-minutes").text();
  var titleSeconds = $(".hero-timer-seconds").text();
  var x = setInterval(function() {
    var now = new Date().getTime();
    var distance = countDownDate - now;
    var days = Math.floor(distance / (1000 * 60 * 60 * 24));
    var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
    var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
    var seconds = Math.floor((distance % (1000 * 60)) / 1000);
    if ((days < 10)||(days == 0)) {days = '0'+days;}
    if ((hours < 10)||(hours == 0)) {hours = '0'+hours;}
    if ((minutes < 10)||(minutes == 0)) {minutes = '0'+minutes;}
    if ((seconds < 10)||(seconds == 0)) {seconds = '0'+seconds;}
    document.getElementById("hero-timer").innerHTML = "<div>"+days+"<span>"+titleDays+"</span></div> <div>"+hours+"<span>"+titleHours+"</span></div> <div>"+minutes+"<span>"+titleMinutes+"</span></div> <div>"+seconds+"<span>"+titleSeconds+"</span></div>";
    if (distance < 0) {
      clearInterval(x);
      document.getElementById("hero-timer").innerHTML = "EXPIRED";
    }
  }, 1000);
});

$(window).resize(function() {
	/*if (window.innerWidth > 767) {
		location.reload();
  }*/
  if ((window.innerWidth < 1137)&&(window.innerWidth > 767)) {
    $('.hero-nav a').click(function(){
      $('.hero-header-right').removeClass('hero-nav-show');
    });
  }
});

/*$(document).scroll(function() {
  var posTop = $(window).scrollTop();
  if (posTop != 0) {
    $('.hero-header').addClass('hero-revealed');
  } else {
    $('.hero-revealed').removeClass('hero-revealed');
  };
});*/
