var gulp = require('gulp'),
    changed = require('gulp-changed'),
    concat = require('gulp-concat'),
    rename = require("gulp-rename"),
	sass = require('gulp-sass'),
	uglify = require('gulp-uglify'),
    autoprefixer = require('gulp-autoprefixer'),
    assetVersion = require('gulp-asset-version');

const chalk = require('chalk');


gulp.task('scss', function () {
    gulp.src('scss/style.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(autoprefixer({
            browsers: ['last 2 version', 'safari 5', 'ie 8', 'ie 9', 'opera 12.1', 'ios 6', 'android 4'],
            cascade: false
        }))
        .pipe(gulp.dest('./'))
        .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
        .pipe(rename('style.min.css'))
        // .pipe(assetVersion())
    	.pipe(gulp.dest('./'));
});


gulp.task('scripts', function() {
    gulp.src('js/script.js')
        .pipe(uglify())
        .pipe(concat('script.min.js'))
        .pipe(changed('js/'))
        .pipe(gulp.dest('js/'));
});


gulp.task('html', function() {
    gulp.src("./index.html")
        .pipe(assetVersion())
        .pipe(gulp.dest('./'));

    gulp.src("./ru/index.html")
        .pipe(assetVersion())
        .pipe(gulp.dest('./ru'));
});


gulp.task('watch', function() {
    gulp.watch('scss/**/*.scss', ['scss']);
    gulp.watch('js/**/*.js', ['scripts']);
});


gulp.task('default', ['watch','scss','scripts','html']);
